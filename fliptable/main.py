import click
import pyperclip

TABLES = {
    "default": "(╯°□°)╯︵ ┻━┻",
    "adorable": "(づ｡◕‿‿◕｡)づ ︵ ┻━┻",
    "battle": "(╯°□°)╯︵ ┻━┻ ︵ ╯(°□° ╯)",
    "bear": "ʕノ•ᴥ•ʔノ ︵ ┻━┻",
    "bomb": "( ・_・)ノ⌒●~*",
    "bored": "(ノ゜-゜)ノ ︵ ┻━┻",
    "buffet": "(╯°□°）╯︵ ┻━━━┻",
    "chill": "┬─┬ ノ( ◕◡◕ ノ)",
    "cry": "(╯'□')╯︵ ┻━┻",
    "cute": "┻━┻ ︵ ლ(⌒-⌒ლ)",
    "dead": "(╯°□°）╯︵ /(x_x)|",
    "eyes": "(👁 ͜ʖ👁) ╯︵ ┻━┻",
    "force": "(._.) ~ ︵ ┻━┻",
    "freakout": "(ﾉಥДಥ)ﾉ︵┻━┻･/",
    "fury": "┻━┻彡 ヽ(ಠДಠ)ノ彡┻━┻",
    "glare": "(╯ಠ_ಠ）╯︵ ┻━┻",
    "hypnotic": "(╯°.°）╯ ┻━┻",
    "jake": "(┛❍ᴥ❍)┛彡┻━┻",
    "laptop": "(ノÒ益Ó)ノ彡▔▔▏",
    "magic": "(/¯◡ ‿ ◡)/¯ ~ ┻━┻",
    "monocle": "(╯ಠ_ರೃ)╯︵ ┻━┻",
    "opposite": "ノ┬─┬ノ ︵ ( \o°o)\\",
    "owl": "(ʘ∇ʘ)ク 彡 ┻━┻",
    "people": "(/ .□.)\ ︵╰(゜Д゜)╯︵ /(.□. \)",
    "person": "(╯°□°）╯︵ /(.□. \)",
    "pudgy": "(ノ ゜Д゜)ノ ︵ ┻━┻",
    "rage": "(ﾉಥ益ಥ）ﾉ ┻━┻",
    "relax": "┬─┬ノ( º _ ºノ)",
    "return": "(ノ^_^)ノ┻━┻ ┬─┬ ノ( ^_^ノ)",
    "robot": "┗[© ♒ ©]┛ ︵ ┻━┻",
    "shrug": "┻━┻ ︵ ¯\(ツ)/¯ ︵ ┻━┻",
    "strong": "/(ò.ó)┛彡┻━┻",
    "tantrum": "┻━┻ ︵ヽ(`Д´)ﾉ︵ ┻━┻",
    "teeth": "(ノಠ益ಠ)ノ彡┻━┻",
    "two": "┻━┻ ︵╰(°□°)╯︵ ┻━┻",
    "whoops": "	┬──┬ ¯\_(ツ)",
    "yelling": "(┛◉Д◉)┛彡┻━┻",
}


DOC = "\n".join((f"{k}: {v}" for k, v in TABLES.items()))


@click.command()
@click.argument("table", nargs=-1, type=str)
def fliptable(table):
    """
    Flips tables.

    \b
    default: (╯°□°)╯︵ ┻━┻
    adorable: (づ｡◕‿‿◕｡)づ ︵ ┻━┻
    battle: (╯°□°)╯︵ ┻━┻ ︵ ╯(°□° ╯)
    bear: ʕノ•ᴥ•ʔノ ︵ ┻━┻
    bomb: ( ・_・)ノ⌒●~*
    bored: (ノ゜-゜)ノ ︵ ┻━┻
    buffet: (╯°□°）╯︵ ┻━━━┻
    chill: ┬─┬ ノ( ◕◡◕ ノ)
    cry: (╯'□')╯︵ ┻━┻
    cute: ┻━┻ ︵ ლ(⌒-⌒ლ)
    dead: (╯°□°）╯︵ /(x_x)|
    eyes: (👁 ͜ʖ👁) ╯︵ ┻━┻
    force: (._.) ~ ︵ ┻━┻
    freakout: (ﾉಥДಥ)ﾉ︵┻━┻･/
    fury: ┻━┻彡 ヽ(ಠДಠ)ノ彡┻━┻
    glare: (╯ಠ_ಠ）╯︵ ┻━┻
    hypnotic: (╯°.°）╯ ┻━┻
    jake: (┛❍ᴥ❍)┛彡┻━┻
    laptop: (ノÒ益Ó)ノ彡▔▔▏
    magic: (/¯◡ ‿ ◡)/¯ ~ ┻━┻
    monocle: (╯ಠ_ರೃ)╯︵ ┻━┻
    opposite: ノ┬─┬ノ ︵ ( \o°o)\\
    owl: (ʘ∇ʘ)ク 彡 ┻━┻
    people: (/ .□.)\ ︵╰(゜Д゜)╯︵ /(.□. \)
    person: (╯°□°）╯︵ /(.□. \)
    pudgy: (ノ ゜Д゜)ノ ︵ ┻━┻
    rage: (ﾉಥ益ಥ）ﾉ ┻━┻
    relax: ┬─┬ノ( º _ ºノ)
    return: (ノ^_^)ノ┻━┻ ┬─┬ ノ( ^_^ノ)
    robot: ┗[© ♒ ©]┛ ︵ ┻━┻
    shrug: ┻━┻ ︵ ¯\(ツ)/¯ ︵ ┻━┻
    strong: /(ò.ó)┛彡┻━┻
    tantrum: ┻━┻ ︵ヽ(`Д´)ﾉ︵ ┻━┻
    teeth: (ノಠ益ಠ)ノ彡┻━┻
    two: ┻━┻ ︵╰(°□°)╯︵ ┻━┻
    whoops: 	┬──┬ ¯\_(ツ)
    yelling: (┛◉Д◉)┛彡┻━┻
    """
    if not table:
        table = ["default"]
    content = " ".join((TABLES.get(t) or TABLES["default"] for t in table))
    pyperclip.copy(content)


fliptable.__doc__ = DOC
